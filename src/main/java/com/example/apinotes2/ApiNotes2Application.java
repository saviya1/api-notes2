package com.example.apinotes2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiNotes2Application {

	public static void main(String[] args) {
		SpringApplication.run(ApiNotes2Application.class, args);
	}

}
